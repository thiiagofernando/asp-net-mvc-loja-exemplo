﻿namespace LojaVirutal.BLL.Departamentos
{
    public interface IRemocaoDeDepartamento
    {
        void Remover(int id);
    }
}
