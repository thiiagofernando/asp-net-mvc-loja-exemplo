﻿using LojaVirutal.BLL._Base;

namespace LojaVirutal.BLL.Departamentos
{
    public interface IDepartamentoRepositorio : IRepositorioBase<Departamento>
    {

    }
}
