﻿using LojaVirtual.Resources;
using System.ComponentModel.DataAnnotations;

namespace LojaVirutal.BLL.Departamentos.Dtos
{
    public class DepartamentoDto
    {
        public int Id { get; set; }

        [Required]
        [StringLength(Resources.QuantidadeCaracter150)]
        [Display(Name = "Nome", Description = "Nome do Departamento")]
        public string Nome { get; set; }

        [StringLength(Resources.QuantidadeCaracter300)]
        [Display(Name = "Descrição")]
        [DataType(DataType.MultilineText)]
        public string Descricao { get; set; }
    }
}
