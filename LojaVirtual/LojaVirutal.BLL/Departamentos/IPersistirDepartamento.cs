﻿using LojaVirutal.BLL.Departamentos.Dtos;

namespace LojaVirutal.BLL.Departamentos
{
    interface IPersistirDepartamento
    {
        Departamento Armazenar(DepartamentoDto dto);
    }
}
