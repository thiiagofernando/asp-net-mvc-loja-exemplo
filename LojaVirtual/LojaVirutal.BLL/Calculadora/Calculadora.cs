﻿using LojaVirutal.BLL._Base;

namespace LojaVirutal.BLL.Calculadora
{
    public class Calculadora : EntidadeBase
    {
        public double PrimeiroValor { get; set; }
        public double SegundoValor { get; set; }

        public int Somar(int valor1, int valor2)
        {
            return valor1 + valor2;
        }
    }
}
