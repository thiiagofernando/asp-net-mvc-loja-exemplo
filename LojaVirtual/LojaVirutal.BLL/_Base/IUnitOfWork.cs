﻿using System;

namespace LojaVirutal.BLL._Base
{
    public interface IUnitOfWork :IDisposable
    {
        void AbrirConexao();
        void Commit();
        void Roolback();
    }
}
